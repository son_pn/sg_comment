import Sequelize from 'sequelize';
import { mysqlConfig } from 'core/mysql_config';

const connection = mysqlConfig.COMMENTDB.database.getConnection();
const CommentLikeModel = {
  system_id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  page_id: {
    type: Sequelize.STRING(50),
    allowNull: false
  },
  post_id: {
    type: Sequelize.STRING(50),
    allowNull: false
  },
  comment_id: {
    type: Sequelize.STRING(50),
    allowNull: false
  },
  sender_name: {
    type: Sequelize.STRING(256),
    allowNull: false
  },
  sender_id: {
    type: Sequelize.BIGINT(20),
    allowNull: false
  },
  system_update_time: {
    type: Sequelize.DATE,
    allowNull: false
    //defaultValue : Sequelize.NOW
  }
};

class MCommentLike {
  constructor(pageId) {
    let tableName = 'sg_pm_' + pageId + '_comments_likes';
    return connection.define(tableName, CommentLikeModel, {
      timestamps: false,
      freezeTableName: true
    });
  }
}

export { MCommentLike };
