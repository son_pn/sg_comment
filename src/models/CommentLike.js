import db from 'core/db';
import Joi from 'joi';
import { promisify } from 'models/helper';

const schema = {
  system_id: db.types.uuid(),
  page_id: Joi.string(),
  post_id: Joi.string(),
  comment_id: Joi.string(),
  sender_name: Joi.string(),
  sender_id: Joi.string(),
  system_updated_time: Joi.string()
};

const tableName = 'CommentLike';
let CommentLike = db.define(tableName, {
  hashKey: 'sender_id',
  rangeKey: 'comment_id',
  timestamps: false,
  schema,
  tableName
});

CommentLike.countLikes = function(comment_id) {
  return promisify(cb =>
    CommentLike.scan()
      .where('comment_id')
      .equals(comment_id)
      .select('COUNT')
      .exec(cb)
  );
};

CommentLike.isLike = function(sender_id, comment_id) {
  return promisify(cb => CommentLike.get({ sender_id: sender_id, comment_id: comment_id }, cb));
};

export { CommentLike };
