import db from 'core/db';
import Joi from 'joi';
import { promisify } from 'models/helper';
import Service from 'core/service';
import { POST_FIELDS } from 'core/constants';

const schema = {
  system_id: db.types.uuid(),
  page_id: Joi.string(),
  post_id: Joi.string(),
  creator_name: Joi.string(),
  creator_id: Joi.string(),
  creator_picture: Joi.string().allow(''),
  message: Joi.string().allow(''),
  attachments: Joi.string().allow(''),
  picture: Joi.string().allow(''),
  full_picture: Joi.string().allow(''),
  link: Joi.string().allow(''),
  created_time: Joi.string(),
  likes_count: Joi.number(),
  is_carousel: Joi.number(),
  first_img: Joi.string().allow(''),
  system_updated_time: Joi.string()
};
const tableName = 'Post';

let Post = db.define(tableName, {
  hashKey: 'page_id',
  rangeKey: 'post_id',
  timestamps: false,
  schema,
  tableName
});

Post.saveData = function(post) {
  return Post.create(post, function(err, post) {
    if (err) {
      console.log(err);
    } else {
      console.log('created comment in DynamoDB', post.attrs);
    }
  });
};

Post.getPost = function(postId) {
  return promisify(cb =>
    Post.scan()
      .where('post_id')
      .equals(postId)
      .exec(cb)
  );
  // return (data.Count > 0) ? data.Items : {};
  // return data;
};

Post.getPostByAccesstoken = async function(postId, pageAccessToken) {
  let fbService = new Service('https://graph.facebook.com/v3.1/');
  let result = fbService
    .getAxios()
    .get(`${postId}?access_token=${pageAccessToken}&fields=${POST_FIELDS}`)
    .then(resData => {
      const { data } = resData;
      if (data) {
        let post = new Object();
        post.post_id = data.hasOwnProperty('id') ? data.id : '';
        if (data.hasOwnProperty('from')) {
          let from = data.from;
          if (from.hasOwnProperty('id')) {
            post.creator_id = from.id;
            post.creator_picture = Post.userPicture(from.id);
          }
          if (post.hasOwnProperty('name')) {
            post.creator_name = from.name;
          }
        }
        post.message = data.hasOwnProperty('message') ? data.message : '';
        post.picture = data.hasOwnProperty('picture') ? data.picture : '';
        post.full_picture = data.hasOwnProperty('full_picture') ? data.full_picture : '';
        post.link = data.hasOwnProperty('link') ? data.link : '';
        post.created_time = data.hasOwnProperty('created_time') ? data.created_time : '';
        post.is_carousel = 0;
        if (data.hasOwnProperty('attachments')) {
          let attachments = data.attachments;
          if (Array.isArray(attachments.data) && attachments.data.length > 0) {
            if (attachments.data[0].hasOwnProperty('type') && attachments.data[0].type == 'multi_share') {
              post.is_carousel = 1;
              let subattachments = attachments.data[0].subattachments.data;
              post.first_img = subattachments[0].media.image.src;
            }
          }
          post.attachments = JSON.stringify(data.attachments);
        }
        return post;
      }
    })
    .catch(error => {
      error.message = `Can't get post data. \r\n${error.message}`;
      throw error;
    });
  return result;
};

Post.userPicture = function(userId) {
  let userImg = `http://graph.facebook.com/${userId}/picture`;
  return userImg;
};

export { Post };
