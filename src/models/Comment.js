import db from 'core/db';
import Joi from 'joi';
import { promisify } from 'models/helper';
import { ITEMS_PER_PAGE } from 'core/constants';

const schema = {
  system_id: db.types.uuid(),
  page_id: Joi.string(),
  post_id: Joi.string(),
  comment_id: Joi.string(),
  sender_name: Joi.string(),
  sender_id: Joi.string().allow(''),
  message: Joi.string().allow(''),
  attachment: Joi.string().allow(''),
  created_time: Joi.string(),
  likes_count: Joi.number(),
  comments_count: Joi.number(),
  type: Joi.any().valid('textmsg', 'imagemsg'),
  is_read: Joi.number(),
  is_important: Joi.number(),
  page_owner: Joi.number(),
  system_updated_time: Joi.string()
};
const tableName = 'Comment';

const Comment = db.define(tableName, {
  hashKey: 'post_id',
  rangeKey: 'comment_id',
  timestamps: false,
  schema,
  indexes: [
    {
      hashKey: 'page_id',
      rangeKey: 'created_time',
      name: 'SortByDateIndex',
      type: 'global'
    }
  ],
  tableName
});

Comment.getComments = function(pageId) {
  return promisify(cb =>
    Comment.query(pageId)
      .usingIndex('SortByDateIndex')
      .descending()
      .exec(cb)
  );
};

Comment.getByCommentId = function(comment_id) {
  return promisify(cb =>
    Comment.scan()
      .where('comment_id')
      .equals(comment_id)
      .attributes(['post_id', 'comment_id'])
      .exec(cb)
  );
};

Comment.getCommentsByPage = async function(pageId, pageNum) {
  let dynamo = new db.AWS.DynamoDB.DocumentClient();
  let params = {
    TableName: 'Comment',
    Limit: ITEMS_PER_PAGE,
    IndexName: 'SortByDateIndex',
    KeyConditionExpression: 'page_id = :p',
    ExpressionAttributeValues: {
      ':p': pageId
    },
    ScanIndexForward: false
  };
  let result = await dynamo.query(params).promise();
  let num = 1;
  while (num < pageNum) {
    if (result.hasOwnProperty('LastEvaluatedKey')) {
      let last = result.LastEvaluatedKey;
      params = {
        TableName: 'Comment',
        Limit: ITEMS_PER_PAGE,
        IndexName: 'SortByDateIndex',
        KeyConditionExpression: 'page_id = :p',
        ExpressionAttributeValues: {
          ':p': pageId
        },
        ExclusiveStartKey: last,
        ScanIndexForward: false
      };
      result = await dynamo.query(params).promise();
    } else {
      result = { Items: [] };
    }
    num++;
  }
  return result;
};
export { Comment };
