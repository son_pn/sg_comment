import Sequelize from 'sequelize';
import { mysqlConfig } from 'core/mysql_config';

const connection = mysqlConfig.COMMENTDB.database.getConnection();
const CommentModel = {
  system_id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  page_id: {
    type: Sequelize.STRING(50),
    allowNull: false
  },
  post_id: {
    type: Sequelize.STRING(50),
    allowNull: false
  },
  comment_id: {
    type: Sequelize.STRING(50),
    allowNull: false
  },
  sender_name: {
    type: Sequelize.STRING(256),
    allowNull: false
  },
  sender_id: {
    type: Sequelize.BIGINT(20),
    allowNull: false
  },
  message: {
    type: Sequelize.TEXT,
    allowNull: false
  },
  attachment: {
    type: Sequelize.TEXT,
    defaultValue: null
  },
  create_time: {
    type: Sequelize.DATE,
    allowNull: false
  },
  likes_count: {
    type: Sequelize.INTEGER(11),
    allowNull: false
  },
  comments_count: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
    defaultValue: 0
  },
  type: {
    type: Sequelize.ENUM,
    values: ['textmsg', 'imagemsg'],
    defaultValue: 'textmsg',
    allowNull: false
  },
  is_read: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: 0
  },
  is_important: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: 0
  },
  page_owner: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: 0
  },
  system_update_time: {
    type: Sequelize.DATE,
    allowNull: false
    //defaultValue : Sequelize.NOW
  }
};

class MComment {
  constructor(pageId) {
    let tableName = 'sg_pm_' + pageId + '_comments';
    return connection.define(tableName, CommentModel, {
      timestamps: false,
      freezeTableName: true
    });
  }
}

export { MComment };
