import { BasePlatform } from "../BasePlatform";
import { POST_FIELDS } from 'core/constants';
import Service from 'core/service';

class FacebookPlatform extends BasePlatform {
    getAPISdk() {
        this.version = 'v3.1';
        this.api = 'https://graph.facebook.com';
        return new Service(`${this.api}/${this.version}/`).getAxios();
    }

    async getPost(postId) {
        let pageAccessToken = this.access_token;
        let fbService = new Service('https://graph.facebook.com/v3.1/');
        let result = fbService
          .getAxios()
          .get(`${postId}?access_token=${pageAccessToken}&fields=${POST_FIELDS}`)
          .then(resData => {
            const { data } = resData;
            if (data) {
              let post = new Object();
              post.post_id = data.hasOwnProperty('id') ? data.id : '';
              if (data.hasOwnProperty('from')) {
                let from = data.from;
                if (from.hasOwnProperty('id')) {
                  post.creator_id = from.id;
                  post.creator_picture = this.user_picture(from.id);
                }
                if (post.hasOwnProperty('name')) {
                  post.creator_name = from.name;
                }
              }
              post.message = data.hasOwnProperty('message') ? data.message : '';
              post.picture = data.hasOwnProperty('picture') ? data.picture : '';
              post.full_picture = data.hasOwnProperty('full_picture') ? data.full_picture : '';
              post.link = data.hasOwnProperty('link') ? data.link : '';
              post.created_time = data.hasOwnProperty('created_time') ? data.created_time : '';
              post.is_carousel = 0;
              if (data.hasOwnProperty('attachments')) {
                let attachments = data.attachments;
                if (Array.isArray(attachments.data) && attachments.data.length > 0) {
                  if (attachments.data[0].hasOwnProperty('type') && attachments.data[0].type == 'multi_share') {
                    post.is_carousel = 1;
                    let subattachments = attachments.data[0].subattachments.data;
                    post.first_img = subattachments[0].media.image.src;
                  }
                }
                post.attachments = JSON.stringify(data.attachments);
              }
              return post;
            }
          })
          .catch(error => {
            error.message = `Can't get post data. \r\n${error.message}`;
            throw error;
          });
        return result;
    };

    async user_picture(userId) {
      return `http://graph.facebook.com/${userId}/picture`;
    };

    async create_comment(id, params) {
      const fbService = new Service('https://graph.facebook.com/v3.0/');
      const { message, attachment_url } = params;
      return await fbService
        .getAxios()
        .post(`${id}/comments`, {
          message: message,
          attachment_url: attachment_url,
          access_token: this.access_token
        })
        .catch(error => {
          error.message = `Can't post commnet. \r\n${error.message}`;
          throw error;
        });
    }

    async delete_comment(id) {
      const fbService = new Service('https://graph.facebook.com/v3.0/');
      return await fbService
        .getAxios()
        .delete(`${id}`, { data: { access_token: this.access_token } })
        .catch(error => {
          console.log(error);
          error.message = `Can't delete comment. \r\n${error.message}`;
          throw error;
      });
    }
}

export { FacebookPlatform };