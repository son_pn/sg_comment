import { FacebookPlatform } from "./facebook/FacebookPlatform";
import { InstagramPlatform } from "./instagram/InstagramPlatform";

class PlatformFactory {
    static getPlatform(name, access_token) {
        switch (name) {
            case 'facebook':
                return new FacebookPlatform(access_token);
            case 'instagram':
                return new InstagramPlatform(access_token);
        }
    }

    getAdapter(name) {
        switch (name) {
            case 'facebook':
                return new FacebookPlatform();
            case 'instagram':
                return new InstagramPlatform();
        }
    }
}

export { PlatformFactory };