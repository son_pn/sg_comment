import { BasePlatform } from "../BasePlatform";
import Request from 'core/request';

class InstagramPlatform extends BasePlatform {
    getAPISdk() {
        this.version = 'v3.1';
        this.api = 'https://graph.facebook.com';
        return new Request(`${this.api}/${this.version}`);
    }

    async getComment(comment_id) {
        const FB = this.getAPISdk();
        try {
            const commentFields = ['id', 'media', 'text', 'timestamp', 'username', 'user', 'hidden'];
            return await FB.get(comment_id + '?fields=' + commentFields.join() + '&access_token=' + this.access_token, {});
        } catch (e) {
            throw new Exception('Error');
        }
    }

    async getComments(media_id, includeReplies = false) {
        const FB = this.getAPISdk();
        try {
            const commentFields = ['id', 'media', 'text', 'timestamp', 'username', 'user', 'hidden'];
            let eagde = media_id + '/comments?fields=' + commentFields.join();
            if (includeReplies) {
                eagde += ",replies{" + commentFields.join() + "}";
            }
            eagde += '&access_token=' + this.access_token;
            let query = eagde;
            let data = null;
            let comments = [];
            let next = false;
            do {
                data = await FB.get(query, {});
                next = data.hasOwnProperty('paging') ? true : false;
                query = (next) ? eagde + '&after=' + data.paging.cursors.after : '';
                comments = [...comments, ...data.data];
            } while (next);
            return comments;
        } catch (e) {
            throw new Exception('Error');
        }
    }

    async getPost(postId) {
        const FB = this.getAPISdk();
        try {
            const mediaFields = ['id', 'caption', 'media_url', 'media_type', 'owner', 'permalink', 'timestamp', 'username', 'children{id,media_type,media_url}'];
            let mediaData = await FB.get(postId + '?fields=' + mediaFields.join() + '&access_token=' + this.access_token, {});
            return mediaData;
        } catch (e) {
            throw new Exception('Error');
        }
    };

    async getUser(user_id) {
        const FB = this.getAPISdk();
        try {
            const userFields = ['id', 'name', 'username', 'profile_picture_url'];
            return await FB.get(user_id + '?fields=' + userFields.join() + '&access_token=' + this.access_token, {});
        } catch (e) {
            throw new Exception('Error');
        }
    }

    async checkIsReply(post_id, comment_id) {
        const FB = this.getAPISdk();
        let next = false;
        let eagde = post_id + '/comments?fields=id,replies{id}' + '&access_token=' + this.access_token;
        let query = eagde;
        let data = null;
        let parent_id = null;
        do {
            data = await FB.get(query, {});
            next = data.hasOwnProperty('paging') ? true : false;
            query = (next) ? eagde + '&after=' + data.paging.cursors.after : '';
            let raw = JSON.stringify(data.data);
            if (raw.indexOf(comment_id) != -1) break;
        } while (next);
        data.data.forEach(element => {
            if (element.id == comment_id) return;
            if (element.hasOwnProperty('replies')) {
                let rawElement = JSON.stringify(element.replies.data);
                if (rawElement.indexOf(comment_id) != -1) {
                    parent_id = element.id;
                    return;
                }
            }
        });
        return parent_id;
    }

    async user_picture(userId) {
        if (!userId) return '';
        let userData = await this.getUser(userId);
        return userData.hasOwnProperty('profile_picture_url') ? userData.profile_picture_url : '';
    };

    async create_comment(id, params) {console.log(id, params);
        const FB = this.getAPISdk();
        try {
            const { message } = params;
            return await FB.rest(id + '/replies', {message: message, access_token: this.access_token});
        } catch (e) {
            throw new Exception('Error');
        }
    }

    async delete_comment(id) {
        const FB = this.getAPISdk();
        try {
            return await FB.rest(id, {access_token: this.access_token}, { method: 'delete' });
        } catch (e) {
            throw new Exception('Error');
        }
    }
}

export { InstagramPlatform };