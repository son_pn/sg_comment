class BasePlatform {
    constructor(access_token) {
        this.access_token = access_token;
    }

    getAPISdk() {
        return { access_token };
    }
}

export { BasePlatform };