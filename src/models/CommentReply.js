import db from 'core/db';
import Joi from 'joi';
import { promisify } from 'models/helper';

const schema = {
  system_id: db.types.uuid(),
  page_id: Joi.string(),
  post_id: Joi.string(),
  comment_id: Joi.string(),
  parent_comment_id: Joi.string(),
  sender_name: Joi.string(),
  sender_id: Joi.string(),
  message: Joi.string().allow(''),
  attachment: Joi.string().allow(''),
  created_time: Joi.string(),
  likes_count: Joi.number(),
  type: Joi.any().valid('textmsg', 'imagemsg'),
  is_read: Joi.number(),
  is_important: Joi.number(),
  page_owner: Joi.number(),
  system_updated_time: Joi.string()
};

const tableName = 'CommentReply';
let CommentReply = db.define(tableName, {
  hashKey: 'parent_comment_id',
  rangeKey: 'comment_id',
  timestamps: false,
  indexes: [
    {
      hashKey: 'parent_comment_id',
      rangeKey: 'created_time',
      name: 'SortByDateIndex',
      type: 'global'
    }
  ],
  schema,
  tableName
});

CommentReply.getReplies = function(parent_comment_id) {
  return promisify(cb =>
    CommentReply.query(parent_comment_id)
      .usingIndex('SortByDateIndex')
      .descending()
      .loadAll()
      .exec(cb)
  );
};
export { CommentReply };
