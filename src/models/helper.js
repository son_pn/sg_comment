const promisify = (cb, transform) => {
  return new Promise((resolve, reject) => {
    cb((err, data) => {
      if (err) return reject(err);

      // case Delete: data will be null
      if (!data)
        return resolve({
          Items: []
        });

      let Items = [];

      // case Create [] data is array Model
      if (Array.isArray(data)) {
        data.map(item => item.attrs && Items.push(item.attrs));
      }
      // case Scan: data.Items is array
      else if (Array.isArray(data.Items)) {
        Items = data.Items.map(itm => itm.attrs);
      }
      // case Insert/Update: data is a Model object
      else if (data.attrs) {
        Items.push(data.attrs);
      }

      const { Count, ScannedCount } = data;
      let res = {
        Items,
        Count,
        ScannedCount
      };

      if (data.hasOwnProperty('LastEvaluatedKey')) {
        let LastEvaluatedKey = data.LastEvaluatedKey;
        res = {
          Items,
          Count,
          ScannedCount,
          LastEvaluatedKey
        };
      }

      transform = transform ? transform : res => res;
      return resolve(transform(res));
    });
  });
};

export { promisify };
