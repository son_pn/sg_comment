import db from 'core/db';
import Joi from 'joi';
import { promisify } from 'models/helper';

var BusinessAccount = db.define('IBusinessAccount', {
  hashKey: 'id',
  timestamps: false,
  schema: {
    id: Joi.string(),
    page_id: Joi.string(),
    access_token: Joi.string(),
    image_url: Joi.string(),
    name: Joi.string()
  },
  tableName: 'IBusinessAccount'
});

BusinessAccount.fetch = function() {
  return promisify(cb =>
    BusinessAccount.scan()
      .exec(cb)
  );
};

export { BusinessAccount };
