import { p } from 'core/constants';

export const WEBHOOK = p('webhook');
export const SEND_MESSAGE = p('sendMessage');
export const CLOSE_CONVERSATION = p('closeConversation');
