import { fb } from 'core/verifier';
import { Response } from 'core/response';
import { Comment } from 'models/Comment';
import { CommentReply } from 'models/CommentReply';
import { CommentLike } from 'models/CommentLike';
import moment from 'moment';
import { Array } from 'core-js';

export const executeFacebookComment = fb(async event => {
  console.log('EXECUTE COMMENT !!!!');
  const res = new Response();
  const inputRequest = JSON.parse(event.body);
  if (
    inputRequest.hasOwnProperty('entry') &&
    Array.isArray(inputRequest.entry) &&
    inputRequest.hasOwnProperty('object') &&
    inputRequest.object === 'page'
  ) {
    await Promise.all(
      inputRequest.entry.map(async entry => {
        let pageId = entry.id;
        if (entry.hasOwnProperty('changes') && Array.isArray(entry.changes)) {
          await Promise.all(
            entry.changes.map(async change => {
              let field = change.field;
              if (change.hasOwnProperty('value')) {
                let value = change.value;
                let itemName = value.item;
                if (field === 'feed') {
                  switch (itemName) {
                    case 'comment':
                      await processComment(value, pageId);
                      break;
                    case 'reaction':
                      if (value.hasOwnProperty('reaction_type')) {
                        if (value.reaction_type == 'like') {
                          await processLikes(value, pageId);
                        }
                      }
                      break;
                  }
                }
              }
            })
          );
        }
      })
    );
  }
  return res.status(200).body({ message: 'EVENT_RECEIVED' });
});

async function processComment(receivedData, pageId) {
  let statustype = receivedData.verb;
  let postId = receivedData.post_id;
  let parentId = receivedData.parent_id;
  let senderId = '';
  let senderName = '';
  if (receivedData.hasOwnProperty('from')) {
    senderId = receivedData.from.hasOwnProperty('id') ? receivedData.from.id : '';
    senderName = receivedData.from.hasOwnProperty('name') ? receivedData.from.name : '';
  } else {
    senderId = receivedData.hasOwnProperty('sender_id') ? receivedData.sender_id : '';
    senderName = receivedData.hasOwnProperty('sender_name') ? receivedData.sender_name : '';
  }
  let commentId = receivedData.comment_id;
  let message = receivedData.hasOwnProperty('message') ? receivedData.message : '';
  let photo = receivedData.hasOwnProperty('photo') ? receivedData.photo : '';
  let video = receivedData.hasOwnProperty('video') ? receivedData.video : '';
  let attachment = '';
  let type = 'textmsg';
  if (photo != '') {
    let photoObj = { photo: photo };
    attachment = JSON.stringify(photoObj);
    type = 'imagemsg';
  }
  if (video != '') {
    let videoObj = { video: video };
    attachment = JSON.stringify(videoObj);
  }
  let createdTime = receivedData.hasOwnProperty('created_time') ? receivedData.created_time : '';
  if (statustype == 'add') {
    let comment = new Object();
    comment.comment_id = commentId;
    createdTime = moment
      .unix(createdTime)
      .utc()
      .format('YYYY-MM-DD H:m:s');
    comment.created_time = createdTime;
    comment.page_id = pageId;
    comment.post_id = postId;
    comment.sender_name = senderName;
    comment.sender_id = senderId;
    comment.message = message;
    comment.attachment = attachment;
    comment.system_updated_time = moment().format('YYYY-MM-DD H:m:s');
    comment.likes_count = 0;
    comment.is_read = 0;
    comment.page_owner = 0;
    comment.type = type;
    if (parentId == postId) {
      return await Comment.create(comment);
    } else {
      comment.parent_comment_id = parentId;
      return await CommentReply.create(comment);
    }
  }
  if (statustype == 'edited') {
    let updatedData = new Object();
    updatedData.comment_id = commentId;
    updatedData.message = message;
    updatedData.created_time = moment
      .unix(createdTime)
      .utc()
      .format('YYYY-MM-DD H:m:s');
    updatedData.system_updated_time = moment().format('YYYY-MM-DD H:m:s');
    updatedData.attachment = attachment;
    if (parentId == postId) {
      updatedData.post_id = postId;
      return await Comment.update(updatedData);
    } else {
      updatedData.parent_comment_id = parentId;
      return await CommentReply.update(updatedData);
    }
  }
  if (statustype == 'remove') {
    let condition = new Object();
    condition.comment_id = commentId;
    if (parentId == postId) {
      condition.post_id = postId;
      return await Comment.destroy(condition);
    } else {
      condition.parent_comment_id = parentId;
      return await CommentReply.destroy(condition);
    }
  }
}

async function processLikes(receivedData, pageId) {
  if (!receivedData.hasOwnProperty('comment_id') || !receivedData.hasOwnProperty('post_id')) {
    return; // skip comment likes
  }
  let statustype = receivedData.verb;
  let postId = receivedData.post_id;
  let commentId = receivedData.comment_id;
  let senderId = '';
  let senderName = '';
  if (receivedData.hasOwnProperty('from')) {
    senderId = receivedData.from.hasOwnProperty('id') ? receivedData.from.id : '';
    senderName = receivedData.from.hasOwnProperty('name') ? receivedData.from.name : '';
  } else {
    senderId = receivedData.hasOwnProperty('sender_id') ? receivedData.sender_id : '';
    senderName = receivedData.hasOwnProperty('sender_name') ? receivedData.sender_name : '';
  }
  if (statustype == 'add') {
    let like = new Object();
    like.sender_id = senderId;
    like.sender_name = senderName;
    like.post_id = postId;
    like.comment_id = commentId;
    like.page_id = pageId;
    like.system_updated_time = moment().format('YYYY-MM-DD H:m:s');
    return await CommentLike.create(like);
  }
  if (statustype == 'remove') {
    let condition = new Object();
    condition.comment_id = commentId;
    condition.sender_id = senderId;
    return await CommentLike.destroy(condition);
  }
}
