var FB = require('fb');
FB.options({version: 'v3.1'});
FB.extend({appId: '179343449328761', appSecret: '834b90001dbf160967cdb2ec94164edd'});
import { Comment } from 'models/Comment';
import { Post } from 'models/Post';
import { BusinessAccount } from 'models/BusinessAccount'; // will be replaced
import moment from 'moment';
import { InstagramPlatform } from 'models/platforms/instagram/InstagramPlatform';
import { CommentReply } from 'models/CommentReply';

class WebhookService {
    /**
     * Register webhook: receive an event from facebook webhook product. 
     * Verify the token and return the challenge code we've received from facebook
     * @param {} event 
     * @return string challenge
     */
    register(event) {
        const VERIFY_TOKEN = "my_verify_token";
        let mode = event.queryStringParameters['hub.mode'];
        let token = event.queryStringParameters['hub.verify_token'];
        let challenge = event.queryStringParameters['hub.challenge'];
        console.log(mode, token, challenge);
        // Check if a token and mode were sent
        if (mode && token) {
            // Check the mode and token sent are correct
            if (mode === 'subscribe' && token === VERIFY_TOKEN) {
            // Respond with 200 OK and challenge token from the request
                console.log('WEBHOOK_VERIFIED');
                
                return challenge;
            }
        }
        return false;
    }

    /**
     * Receive an structured object of new comment sent by facebook webhook, 
     * analize and save relevant data to dynamodb. 
     * If the post of the comment is exist in this system, we just save the new comment
     * If the post of the comment is not exist before in this system, we fetch all its 
     * comments and save to dynamo, included the newest one.
     * 
     * @param {} object 
     *  {
     *    changes: [{
     *      "field": "comments",
     *      "value": {
     *        "id": "17865799348089039",
     *        "text": "This is an example."
     *      }
     *    }]
     *  }
     * @return any
     */
    async processComment(object) {
        const business_account_id = object.id;
        const access_token = this.getAccessToken(business_account_id);
        const platform = new InstagramPlatform(access_token);
        const change = object.changes[0];
        const comment_id = change.value.id;
        const commentData = await platform.getComment(comment_id);
        const media_id = commentData.media.id + '';
        const media = await Post.getPost(media_id);
        if (!media.Count) {
            const mediaData = await platform.getPost(media_id);
            if (mediaData.hasOwnProperty('owner')) {
                mediaData.creator_picture = await platform.user_picture(mediaData.owner.id);
            }
            await this.savePostToDynamoDb(business_account_id, mediaData);
            // let mediaComments = await platform.getComments(media_id, true);
            // mediaComments.forEach(async (element) => {
            //     this.saveCommentToDynamoDb(business_account_id, element);
            //     let replies = element.hasOwnProperty('replies') ? element.replies.data : [];
            //     if (replies.length) {
            //         replies.forEach(async (reply) => {
            //             await this.saveReplyToDynamoDb(business_account_id, element.id, reply);
            //         });
            //     };
            //     return;
            // });
            // return post;
        }
        let parentCommentId = await platform.checkIsReply(media_id, comment_id);
        if (parentCommentId) { // is reply
            // save parent comment first
            const parentCommentData = await platform.getComment(parentCommentId);
            await this.saveCommentToDynamoDb(business_account_id, parentCommentData);
            // save the reply
            return await this.saveReplyToDynamoDb(business_account_id, parentCommentId, commentData);
        } else { // is comment
            return await this.saveCommentToDynamoDb(business_account_id, commentData);
        }
    }

    async getAccessToken(business_account_id) { // This function will be deprecated.
        const business = await BusinessAccount.get({'id': business_account_id});
        return business.get('access_token');
    }

    /**
     * Save a structured object to dynamodb
     * 
     * @param string page_id
     * @param string media_id
     * @param Object commentData
     * {
     *   "id": "17967029188243331",
     *   "text": "is this a second hand?",
     *   "media": {
     *     "id": "17868861472335100"
     *   },
     *   "username": "sonpham1988",
     *   "user": {
     *     "id": "17841401393335080"
     *   },
     *   "hidden": false,
     *   "timestamp": "2019-03-14T09:54:38+0000",
     *   "sender_picture_url": "url"
     * }
     * @return Object Comment
     */
    async saveCommentToDynamoDb(business_account_id, commentData) {
        let comment = {};
        comment.page_id = business_account_id;
        comment.post_id = commentData.media.id;
        comment.comment_id = commentData.id;
        comment.sender_name = commentData.username;
        comment.sender_id = commentData.hasOwnProperty('user') ? commentData.user.id : '';
        comment.message = commentData.text;
        comment.attachment = '';
        comment.created_time = moment(commentData.timestamp).utc().format('YYYY-MM-DD H:m:s');
        comment.likes_count = 0;
        comment.is_read = 0;
        comment.is_important = 0;
        comment.page_owner = 0;
        comment.type = 'textmsg';
        comment.system_updated_time = moment().format('YYYY-MM-DD H:m:s');
        return await Comment.create(comment);
    }

    /**
     * Save a structured object to dynamodb
     * 
     * @param string page_id
     * @param string media_id
     * @param Object commentData
     * {
     *   "id": "17967029188243331",
     *   "text": "is this a second hand?",
     *   "media": {
     *     "id": "17868861472335100"
     *   },
     *   "username": "sonpham1988",
     *   "user": {
     *     "id": "17841401393335080"
     *   },
     *   "hidden": false,
     *   "timestamp": "2019-03-14T09:54:38+0000",
     *   "sender_picture_url": "url"
     * }
     * @return Object Comment
     */
    async saveReplyToDynamoDb(business_account_id, parent_comment_id, commentData) {
        let comment = {};
        comment.page_id = business_account_id;
        comment.post_id = commentData.media.id;
        comment.comment_id = commentData.id;
        comment.parent_comment_id = parent_comment_id;
        comment.sender_name = commentData.username;
        comment.sender_id = commentData.hasOwnProperty('user') ? commentData.user.id : '';
        comment.message = commentData.text;
        comment.attachment = '';
        comment.created_time = moment(commentData.timestamp).utc().format('YYYY-MM-DD H:m:s');
        comment.likes_count = 0;
        comment.is_read = 0;
        comment.is_important = 0;
        comment.page_owner = 0;
        comment.type = 'textmsg';
        comment.system_updated_time = moment().format('YYYY-MM-DD H:m:s');
        return await CommentReply.create(comment);
    }

    /**
     * Save a structured object to dynamodb
     * 
     * @param string page_id
     * @param Object mediaData
     * {
     *   "id": "17868861472335100",
     *   "caption": "Caption",
     *   "media_url": "https://scontent.xx.fbcdn.net/v/t51.2885-15/52416053_390745018376514_2595381841881346673_n.jpg?_nc_cat=101&_nc_ht=scontent.xx&oh=93f2392778afcc7b8c20676e93c2d614&oe=5D0F36A6",
     *   "owner": {
     *     "id": "17841401393335080"
     *   },
     *   "creator_picture": "url"
     *   "permalink": "https://www.instagram.com/p/Bu6rqL4BDix/",
     *   "timestamp": "2019-03-12T17:37:50+0000",
     *   "username": "sonpham1988"
     * }
     * @return Object Post
     */
    async savePostToDynamoDb(business_account_id, mediaData) {
        let post = {};
        const carousel = (mediaData.media_type == 'CAROUSEL_ALBUM') ? 1 : 0;
        let attachment = null;
        if (carousel) {
            let casourelData = [];
            mediaData.children.data.forEach(child => {
                let casourelItem = {
                    media: {
                        image: {src: child.media_url}
                    }
                };
                casourelData.push(casourelItem);
            });
            attachment = {data: [{
                type: 'album',
                title: mediaData.caption,
                media: {image: {src: mediaData.media_url}},
                url: mediaData.permalink,
                subattachments: {
                    data: casourelData
                }
            }]};
        } else {
            attachment = {data: [{
                type: carousel ? 'carousel' : 'photo',
                title: mediaData.caption,
                media: {image: {src: mediaData.media_url}},
                url: mediaData.permalink,
            }]};
        }
        post.page_id = business_account_id;
        post.post_id = mediaData.id;
        post.creator_name = mediaData.username;
        post.creator_id = mediaData.hasOwnProperty('owner') ? mediaData.owner.id : '';
        post.creator_picture = mediaData.hasOwnProperty('creator_picture') ? mediaData.creator_picture : '';
        post.message = mediaData.caption;
        post.attachments = JSON.stringify(attachment);
        post.picture = mediaData.media_url;
        post.full_picture = mediaData.media_url;
        post.link = mediaData.permalink;
        post.created_time = moment(mediaData.timestamp).utc().format('YYYY-MM-DD H:m:s');
        post.likes_count = mediaData.like_count;
        post.is_carousel = carousel;
        post.first_img = mediaData.media_url;
        post.system_updated_time = moment().format('YYYY-MM-DD H:m:s');
        return await Post.create(post);
    }
}

export { WebhookService };
