import { WebhookService } from './webhook';
import { Response } from 'core/response';

const res = new Response();
const webhook = new WebhookService();

export const listener = async (event, ctx, cb) => {
    if (!event.httpMethod) {
        return res.internalError({message: 'No http method found'});
    }

    switch (event.httpMethod) {
        case 'GET':
            let result = webhook.register(event);
            if (!result) return res.internalError({message: 'Can not register webhook'});//Content-type
            // await db.createTables({
            //     'Comment': {readCapacity: 5, writeCapacity: 10},
            //     'Post': {readCapacity: 20, writeCapacity: 4}
            // },
            // function(err) {
            //     if (err) {
            //       console.log('Error creating tables: ', err);
            //     } else {
            //       console.log('All Tables has been created');
            //     }
            // });
            return res.status(200).text(result);
        case 'POST':
            let payload = JSON.parse(event.body);
            if (payload.object != "instagram") return res.status(200);
            const object = payload.entry[0];
            const change = object.changes[0];
            if (change.field != 'comments') return res.status(200);
            await webhook.processComment(object);
            return res.status(200).body({message: 'ok'});
        default:
            return res.internalError({message: 'No action found'});
    }
};
