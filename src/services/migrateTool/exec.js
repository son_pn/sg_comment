import { Response } from 'core/response';
import { Comment } from 'models/Comment';
import { MComment } from 'models/MComment';
import { CommentReply } from 'models/CommentReply';
import { MCommentReply } from 'models/MCommentReply';
import { CommentLike } from 'models/CommentLike';
import { MCommentLike } from 'models/MCommentLike';
import moment from 'moment';
import { MIGRATE_VERIFY_TOKEN } from 'core/constants';

export const migrateComments = async event => {
  const inputRequest = JSON.parse(event.body);
  let pageIds = inputRequest.pages;
  let keyVerify = inputRequest.key;
  if (keyVerify == MIGRATE_VERIFY_TOKEN) {
    await Promise.all(
      pageIds.map(async pageId => {
        let mysql_comments = await new MComment(pageId).findAll();
        await Promise.all(
          mysql_comments.map(async mysql_comment => {
            let cmData = mysql_comment.dataValues;
            cmData.sender_id = cmData.sender_id.toString();
            cmData.is_read = cmData.is_read ? 1 : 0;
            cmData.is_important = cmData.is_important ? 1 : 0;
            cmData.page_owner = cmData.page_owner ? 1 : 0;
            cmData.created_time = moment.utc(cmData.create_time).format('YYYY-MM-DD H:m:s');
            cmData.system_updated_time = moment.utc(cmData.system_update_time).format('YYYY-MM-DD H:m:s');

            delete cmData['system_id'];
            delete cmData['create_time'];
            delete cmData['system_update_time'];

            await Comment.saveCommentData(cmData);
          })
        );
      })
    );
    let res = new Response();
    let success = { message: 'Migrated comment !' };
    return res.body(success);
  } else {
    let res = new Response();
    let error = { message: 'Missing or invalid migration key' };
    return res.body(error);
  }
};

export const migrateCommentReplies = async event => {
  const inputRequest = JSON.parse(event.body);
  let pageIds = inputRequest.pages;
  let keyVerify = inputRequest.key;
  if (keyVerify == MIGRATE_VERIFY_TOKEN) {
    await Promise.all(
      pageIds.map(async pageId => {
        let mysql_comment_replies = await new MCommentReply(pageId).findAll();
        await Promise.all(
          mysql_comment_replies.map(async mysql_comment_reply => {
            let cmData = mysql_comment_reply.dataValues;
            cmData.sender_id = cmData.sender_id.toString();
            cmData.is_read = cmData.is_read ? 1 : 0;
            cmData.is_important = cmData.is_important ? 1 : 0;
            cmData.page_owner = cmData.page_owner ? 1 : 0;
            cmData.created_time = moment.utc(cmData.create_time).format('YYYY-MM-DD H:m:s');
            cmData.system_updated_time = moment.utc(cmData.system_update_time).format('YYYY-MM-DD H:m:s');

            delete cmData['system_id'];
            delete cmData['create_time'];
            delete cmData['system_update_time'];

            await CommentReply.saveCommentReplyData(cmData);
          })
        );
      })
    );
    let res = new Response();
    let success = { message: 'Migrated comment replies!' };
    return res.body(success);
  } else {
    let res = new Response();
    let error = { message: 'Missing or invalid migration key' };
    return res.body(error);
  }
};

export const migrateCommentLikes = async event => {
  const inputRequest = JSON.parse(event.body);
  let pageIds = inputRequest.pages;
  let keyVerify = inputRequest.key;
  if (keyVerify == MIGRATE_VERIFY_TOKEN) {
    await Promise.all(
      pageIds.map(async pageId => {
        let mysql_comment_likes = await new MCommentLike(pageId).findAll();
        await Promise.all(
          mysql_comment_likes.map(async mysql_comment_like => {
            let likeData = mysql_comment_like.dataValues;
            likeData.sender_id = likeData.sender_id.toString();
            likeData.system_updated_time = moment.utc(likeData.system_update_time).format('YYYY-MM-DD H:m:s');

            delete likeData['system_id'];
            delete likeData['system_update_time'];

            await CommentLike.saveCommentLikeData(likeData);
          })
        );
      })
    );
    let res = new Response();
    let success = { message: 'Migrated likes !' };
    return res.body(success);
  } else {
    let res = new Response();
    let error = { message: 'Missing or invalid migration key' };
    return res.body(error);
  }
};
