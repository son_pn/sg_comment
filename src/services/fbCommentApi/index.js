import { fb } from 'core/verifier';
import { Response } from 'core/response';
import { Comment } from 'models/Comment';
import { CommentReply } from 'models/CommentReply';
import { CommentLike } from 'models/CommentLike';
import { Post } from 'models/Post';
import Service from 'core/service';
import db from 'core/db';
import busboy from 'busboy';
// import { SpeedTest } from 'models/SpeedTest';
import { PlatformFactory } from 'models/platforms/PlatformFactory';

export const comments = async (event, ctx, cb) => {
  let request = {};
  let res = new Response();
  if (event.hasOwnProperty('queryStringParameters')) {
    request = event.queryStringParameters;
  }
  if (!request.hasOwnProperty('page_id') || !request.hasOwnProperty('access_token')) {
    return res.body({ message: 'page_id and access_token are required field' });
  }
  let pageId = request.page_id;
  let access_token = request.access_token;
  let type = request.type;
  let platform = PlatformFactory.getPlatform(type, access_token);console.log(platform);
  let page = request.hasOwnProperty('paging') ? request.paging : parseInt(1);
  let data = await Comment.getCommentsByPage(pageId, page);
  let comments = data.Items;
  await Promise.all(
    comments.map(async comment => {
      let { Count } = await CommentLike.countLikes(comment.comment_id);
      comment.likes_count = Count;
      let result = await CommentLike.isLike(pageId, comment.comment_id);
      comment.is_like = 0;
      if (Array.isArray(result.Items) && result.Items.length > 0) comment.is_like = 1;
      comment.create_time = comment.created_time;
      let post = await Post.getPost(comment.post_id);
      if (post.Count == 0) {
        post = await platform.getPost(comment.post_id);
        post.page_id = comment.page_id;
        await Post.saveData(post);
        comment.post_detail = post;
      } else {
        comment.post_detail = post.Items[0];
      }
      comment.userpicture = await platform.user_picture(comment.sender_id);
      let replyList = await CommentReply.getReplies(comment.comment_id);
      comment.reply_list = [];
      if (replyList.hasOwnProperty('Items') && replyList.Items.length > 0) {
        await Promise.all(
          replyList.Items.map(async reply => {
            reply.userpicture = await platform.user_picture(reply.sender_id);
            reply.create_time = reply.created_time;
            let { Count } = await CommentLike.countLikes(reply.comment_id);
            reply.likes_count = Count;
            let result = await CommentLike.isLike(pageId, reply.comment_id);
            reply.is_like = 0;
            if (Array.isArray(result.Items) && result.Items.length > 0) reply.is_like = 1;
          })
        );
        comment.reply_list = replyList.Items;
      }
    }));
  return res.status(200).body(comments);
}

export const updateComment = async (event, ctx, cb) => {
  let res = new Response();
  let pathPrs = {};
  if (event.hasOwnProperty('pathParameters')) {
    pathPrs = event.pathParameters;
  }

  let contentType = event.headers['Content-Type'] || event.headers['content-type'];
  let bb = new busboy({ headers: { 'content-type': contentType } });
  let request = {};
  bb.on('field', (fieldname, val) => (request[fieldname] = val));
  await bb.end(event.body);

  if (!request.access_token || !request.message) {
    return res.body({ message: 'message and access_token are required field' });
  }
  if (request.type == 'instagram') res.body({ message: 'Instagram does not support updating' }); // instagram doesn't support updating

  const fbService = new Service('https://graph.facebook.com/v3.0/');
  let accessToken = request.access_token;
  let message = request.message;
  let attachment_url = '';
  if (request.attachment_url) {
    attachment_url = request.attachment_url;
  }
  if (pathPrs.comment_id) {
    let comment_id = pathPrs.comment_id;
    return await fbService
      .getAxios()
      .post(`${comment_id}`, {
        message: message,
        attachment_url: attachment_url,
        access_token: accessToken
      })
      .then(resData => {
        let success = { message: 'Update comment successful' };
        return res.body({ success });
      })
      .catch(error => {
        error.message = `Can't update commnet. \r\n${error.message}`;
        throw error;
      });
  }
};

export const registerPage = async (event, ctx, cb) => {
  await db.createTables(function(err) {
    if (err) {
      console.log('Error creating tables: ', err);
    } else {
      console.log('All Tables has been created');
    }
  });
  let res = new Response();
  return res.body({ data: 'sccucess' });
};

export const comment = async (event, ctx, cb) => {
  let res = new Response();
  let contentType = event.headers['Content-Type'] || event.headers['content-type'];
  let bb = new busboy({ headers: { 'content-type': contentType } });
  let request = {};
  bb.on('field', (fieldname, val) => (request[fieldname] = val));
  await bb.end(event.body);

  if (!request.access_token || !request.message) {
    return res.body({ message: 'message and access_token are required field' });
  }
  let access_token = request.access_token;
  let message = request.message;
  let attachment_url = '';
  if (request.attachment_url) {
    attachment_url = request.attachment_url;
  }
  let type = request.type;
  let platform = PlatformFactory.getPlatform(type, access_token);
  if (request.post_id) {
    let post_id = request.post_id;
    let result = await platform.create_comment(post_id, {message: message, attachment_url: attachment_url});
    res.body(result);
  }

  return res.body({ message: 'post_id or comment_id is required!' });
};

export const deleteComment = async (event, ctx, cb) => {
  let res = new Response();
  let request = {};
  if (event.hasOwnProperty('queryStringParameters')) {
    request = event.queryStringParameters;
  }
  if (!request.hasOwnProperty('comment_id') || !request.hasOwnProperty('access_token')) {
    return res.body({ message: 'comment_id and access_token are required field' });
  }
  let comment_id = request.comment_id;
  let access_token = request.access_token;
  let type = request.type;
  let platform = PlatformFactory.getPlatform(type, access_token);
  let result = await platform.delete_comment(comment_id);
  return res.body(result);
};

export const like = async (event, ctx, cb) => {
  let res = new Response();
  let contentType = event.headers['Content-Type'] || event.headers['content-type'];
  let bb = new busboy({ headers: { 'content-type': contentType } });
  let request = {};
  bb.on('field', (fieldname, val) => (request[fieldname] = val));
  await bb.end(event.body);

  if (!request.access_token || !request.comment_id) {
    return res.body({ message: 'comment_id and access_token are required field' });
  }
  let comment_id = request.comment_id;
  let accessToken = request.access_token;
  if (request.type == 'instagram') res.body({ message: 'Instagram does not support like comment function' }); // Instagram does not support like comment function
  
  const fbService = new Service('https://graph.facebook.com/v3.0/');
  return await fbService
    .getAxios()
    .post(`${comment_id}/likes`, { access_token: accessToken })
    .then(resData => {
      let success = { message: 'like comment successful', data: {} };
      return res.body({ success });
    })
    .catch(error => {
      error.message = `Can't like comment. \r\n${error.message}`;
      throw error;
    });
};

export const dislike = async (event, ctx, cb) => {
  let res = new Response();
  let contentType = event.headers['Content-Type'] || event.headers['content-type'];
  let bb = new busboy({ headers: { 'content-type': contentType } });
  let request = {};
  bb.on('field', (fieldname, val) => (request[fieldname] = val));
  await bb.end(event.body);

  if (!request.access_token || !request.comment_id) {
    return res.body({ message: 'comment_id and access_token are required field' });
  }
  let comment_id = request.comment_id;
  let accessToken = request.access_token;
  if (request.type == 'instagram') res.body({ message: 'Instagram does not support dislike comment function' }); // Instagram does not support dislike comment function
  const fbService = new Service('https://graph.facebook.com/v3.0/');
  return await fbService
    .getAxios()
    .delete(`${comment_id}/likes`, { data: { access_token: accessToken } })
    .then(resData => {
      let success = { message: 'dislike comment successful' };
      return res.body({ success });
    })
    .catch(error => {
      error.message = `Can't dislike comment. \r\n${error.message}`;
      throw error;
    });
};

export const read = async (event, ctx, cb) => {
  let pathPrs = {};
  if (event.hasOwnProperty('pathParameters')) {
    pathPrs = event.pathParameters;
  }
  if (pathPrs.comment_id) {
    let updatedData = new Object();
    updatedData.comment_id = pathPrs.comment_id;
    updatedData.is_read = 1;
    let data = await Comment.getByCommentId(pathPrs.comment_id);
    if (data.hasOwnProperty('Items') && data.Items.length > 0) {
      updatedData.post_id = data.Items[0].post_id;
      await Comment.update(updatedData);
    }
  }
  let res = new Response();
  let success = { message: 'update read', data: {} };
  return res.body({ success });
};
