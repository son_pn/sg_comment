export const isDevelopment = () => process.env.STAGE === 'dev';
