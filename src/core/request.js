import axios from 'axios';

export default class Request {
  /**
   * Creates an instance of Service.
   *
   * @memberOf Request
   */
  constructor(url = null) {
    const baseURL = url || '';
    this.axios = axios.create({
      baseURL: `${baseURL}/`,
      responseType: 'json'
    });
  }

  getAxios() {
    return this.axios;
  }
  /**
   * Call a service action via REST API
   *
   * @param {any} action  name of action
   * @param {any} params  parameters to request
   * @returns  {Promise}
   *
   * @memberOf Service
   */
  async rest(action, params, options = { method: 'post' }) {
    try {
      const response = await this.axios.request(action, {
        method: options.method,
        data: params
      });
      const { data } = response;
      return data;
    } catch (err) {
      // TODO: handle error
      throw err;
    }
  }

  get(action, params) {
    return this.rest(action, params, { method: 'get' });
  }
}
