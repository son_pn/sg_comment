import Sequelize from 'sequelize';
export const mysqlConfig = {
  COMMENTDB: {
    database: {
      host: 'localhost',
      username: 'root',
      password: '',
      database: 'analytics_postmanagement',
      protocol: 'mysql',
      port: '3306',
      getConnection: function() {
        return new Sequelize(this.database, this.username, this.password, {
          dialect: this.protocol,
          host: this.host,
          port: this.port,
          charset: 'utf8mb4'
        });
      }
    }
  }
};
