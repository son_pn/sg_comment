import { OK, NOT_FOUND } from 'http-status-codes';
import { FB_VERIFY_TOKEN } from 'core/constants';

export const fb = func => async (event, ctx, cb) => {
  const { queryStringParameters: params } = event;
  const token = params && params['hub.verify_token'];
  if (token) {
    if (token === FB_VERIFY_TOKEN) {
      const challenge = params['hub.challenge'];
      return {
        statusCode: OK,
        body: JSON.stringify(parseInt(challenge))
      };
    } else {
      return {
        statusCode: NOT_FOUND,
        body: 'Error, wrong validation token'
      };
    }
  }
  return func(event, ctx, cb);
};
