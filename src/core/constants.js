export const PREFIX = 'cplus';
export const p = k => `${PREFIX}#${k}`;
export const UNAUTHORIZED = p('UNAUTHORIZED');
export const BAD_REQUEST = p('BAD_REQUEST');
export const FB_VERIFY_TOKEN = '123456789';
export const RES_EMPTY_ARRAY = {
  Items: [],
  Count: 0,
  ScannedCount: 0
};
export const POST_FIELDS =
  'application,caption,coordinates,created_time,description,expanded_height,expanded_width,feed_targeting,from,full_picture,height,icon,id,is_hidden,is_published,link,message,message_tags,name,object_id,parent_id,picture,place,privacy,promotion_status,properties,scheduled_publish_time,shares,source,status_type,story,story_tags,subscribed,targeting,timeline_visibility,to,type,updated_time,via,width,with_tags,attachments';
export const ITEMS_PER_PAGE = parseInt(10);
export const MIGRATE_VERIFY_TOKEN = 'CPLUS_CMM_MIGRATION_ABC123';
